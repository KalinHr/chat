QT += core gui
QT += widgets
QT += network

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    message.cpp \
    messagesmodel.cpp \
    communicationmanager.cpp

HEADERS += \
    mainwindow.h \
    message.h \
    messagesmodel.h \
    communicationmanager.h
