#include <QColor>

#include "messagesmodel.h"

MessagesModel::MessagesModel(QObject *parent)
    :QAbstractTableModel(parent)
{
}

int MessagesModel::rowCount(const QModelIndex & /*parent*/) const
{
    return m_messages.count();
}

int MessagesModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 2;
}

QVariant MessagesModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= m_messages.size())
        return QVariant();

    if (role == Qt::DisplayRole)
    {
        return m_messages.at(index.row())->GetData(index.column() + 1);
    }

    if (m_messages.at(index.row())->GetData(0) == false)
    {
        if (role == Qt::BackgroundRole)
        {
           return QVariant(QColor(102, 178, 255, 255));
        }

        if (role == Qt::ForegroundRole)
        {
           return QVariant(QColor(240, 240, 240, 255));
        }
    }

    return QVariant();
}

QVariant MessagesModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();

    if(orientation == Qt::Horizontal)
    {
         switch(section)
         {
             case 0:
                 return QString("Sender");
             case 1:
                 return QString("Message");
        }
    }

    return QVariant();
}

void MessagesModel::addMessage(Message* aMessage)
{
    beginInsertRows( QModelIndex() , rowCount(), rowCount() );
    m_messages.push_back(new Message(aMessage->GetData(0).toBool(), aMessage->GetData(1).toInt(), aMessage->GetData(2).toString()));
    endInsertRows();
}
