#ifndef MESSAGE_H
#define MESSAGE_H

#include <QString>
#include <QVariant>

class Message
{
public:
    Message(bool aSender, int aPort, QString aText);

    QVariant GetData(int column) const;

private:
    bool m_sender;
    int m_port;
    QString m_text;
};

#endif // MESSAGE_H
