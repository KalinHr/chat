#ifndef MESSAGESTABLE_H
#define MESSAGESTABLE_H

#include "message.h"

#include <QAbstractTableModel>

class MessagesModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit MessagesModel(QObject *parent = Q_NULLPTR);

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation = Qt::Horizontal,
                        int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

    void addMessage(Message* aMessage);

private:
    QList<Message*> m_messages;
};

#endif // MESSAGESTABLE_H
