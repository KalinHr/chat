#include "communicationmanager.h"

#include <QShortcut>
#include <QMessageBox>
#include <QNetworkInterface>

CommunicationManager::CommunicationManager(QWidget *parent)
    : QWidget(parent)
    , m_UI(new MainWindow)
    , m_server(new QTcpServer)
    , m_tcpSocket(Q_NULLPTR)
    , m_messagesModel(new MessagesModel)
{
    if (!m_server->listen()) {
        close();
        return;
    }

    QString serverAddress;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // use the first non-localhost IPv4 address
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
            ipAddressesList.at(i).toIPv4Address()) {
            serverAddress = ipAddressesList.at(i).toString();
            break;
        }
    }
    // if we did not find one, use IPv4 localhost
    if (serverAddress.isEmpty())
        serverAddress = QHostAddress(QHostAddress::LocalHost).toString();

    m_UI->setWindowTitle(tr("%1 : %2").arg(serverAddress).arg(m_server->serverPort()));
    m_UI->setModel(m_messagesModel);

    m_tcpSocket = new QTcpSocket(this);

    connect(m_server, SIGNAL(newConnection()), this, SLOT(connectionAccepted()));

    connect(m_UI, SIGNAL(connectButtonSignal()), this, SLOT(connectButtonClicked()));
    connect(m_UI, SIGNAL(sendMessageSignal()), this, SLOT(sendMessageClicked()));
    connect(m_tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(displayError(QAbstractSocket::SocketError)));
    connect(m_messagesModel, SIGNAL(rowsInserted(QModelIndex,int,int)), this, SLOT(scrollTable()));
    connect(m_tcpSocket, SIGNAL(connected()), this, SLOT(socketIsConnected()));
    connect(m_tcpSocket, SIGNAL(disconnected()), this, SLOT(socketIsDisconnected()));

    QShortcut* shortcut = new QShortcut(QKeySequence(tr("Ctrl+Return")), this);
    connect (shortcut, SIGNAL(activated()), this, SLOT(sendMessageClicked()));
}

void CommunicationManager::connectionAccepted(){
    QTcpSocket* socket = m_server->nextPendingConnection();
    connect(socket, SIGNAL(readyRead()), this, SLOT(readSocket()));
}

void CommunicationManager::readSocket(){
    QTcpSocket *socket = static_cast<QTcpSocket*>(sender());
    QByteArray buffer = socket->readAll();

    m_messagesModel->addMessage(new Message(false, m_UI->portEditor()->text().toInt(), buffer));

    qDebug() << "readSocket";
}

void CommunicationManager::connectButtonClicked()
{
    m_UI->connectButton()->setText("...");

    if (m_tcpSocket->isOpen()) {
        m_tcpSocket->close();
    }
    else {
        m_tcpSocket->abort();
        m_tcpSocket->connectToHost(m_UI->ipEditor()->text(), m_UI->portEditor()->text().toInt());
    }

    qDebug() << "connectButtonPressed";
}

void CommunicationManager::socketIsConnected() {
    m_UI->connectButton()->setText("Disconnect");
}

void CommunicationManager::socketIsDisconnected() {
    m_UI->connectButton()->setText("Connect");
}

void CommunicationManager::sendMessageClicked()
{
    QByteArray data (m_UI->messageEditor()->toPlainText().toStdString().c_str());

    m_tcpSocket->write(data);
    m_messagesModel->addMessage(new Message(true,m_server->serverPort(), data));

    qDebug() << "sendMessage";
}

void CommunicationManager::displayError(QAbstractSocket::SocketError socketError)
{
    m_tcpSocket->close();
    m_UI->connectButton()->setText("Connect");

    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr(""),
                                 tr("The host was not found."));
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, tr(""),
                                 tr("The connection was refused by the peer. "));
        break;
    default:
        QMessageBox::information(this, tr(""),
                                 tr("The following error occurred: %1.")
                                 .arg(m_tcpSocket->errorString()));
    }

    qDebug() << "Error";
}

void CommunicationManager::scrollTable(){
    m_UI->scrollTable();
}

MainWindow* CommunicationManager::ui() {
    return m_UI;
}
